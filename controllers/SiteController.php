<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\UsersForm;
use app\models\Hiscores;
use app\models\Jumperhs;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) { //if not guest go back to index.php starting page, so you can't log in twice
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) { //login() function validates the login using Users.php (stated in config/web.php) and LoginForm.php 
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }


    /**
     * SignUp action.
     *
     * @return string
     */
    public function actionSignup()
    {
        $model = new UsersForm();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                $model->password_hash = password_hash($model->password_hash, PASSWORD_BCRYPT);//encrypts the password before saving it to database
                $model->save($model->attributes); //save information to database
                return $this->render('index');
            }
        }
        //if registeration fails return to signup form
        return $this->render('SignUp', [
            'model' => $model,
        ]);
    }
    /**
     * Displays Profile page.
     *
     * @return string
     */    
    public function actionProfile()
    {
        if (Yii::$app->user->isGuest) { //if guest go back to index.php starting page
            return $this->goHome();
        }
        else{
           return $this->render('profile');
        }
    }

    /**
     * Displays Weather page.
     * Weather is available even if not logged in
     * @return string
     */
    public function actionWeather()
    {
        return $this->render('weather');
    }
     /**
     * Displays Multiplayer page.
     *
     * @return string
     */
    public function actionMultiplayer()
    {
        if (Yii::$app->user->isGuest) { //if guest go back to index.php starting page
            return $this->goHome();
        }
        else{
            return $this->render('multiplayer');
        }
    }
    /**
     * Displays Singleplayer page.
     *
     * @return string
     */
    public function actionSingleplayer()
    {
        if (Yii::$app->user->isGuest) { //if guest go back to index.php starting page
            return $this->goHome();
        }
        else{
            return $this->render('singleplayer');
        }
    }
    /*
     * Handless the Ajax-request from game2.js to save score to database
     */
    public function actionSetscore()
    {
        $model = new Hiscores();
        //saves different attributes to model
        $model->user_id = Yii::$app->user->identity->user_id;
        $model->username = Yii::$app->user->identity->username;
        $model->score = Yii::$app->request->post("score");
        if($model->validate()){ //validates the model and saves information to database
            $model->save($model->attributes);  
            $success = true;
        }
        else{
            $success = false;
        }
        return $success; // returns true or false depending if succesfull
    }
    /*
     * Handless the Ajax-request from game2.js to load hiscore table in json
     */
    public function actionGetscore()
    {
        $model = new Hiscores();
        $responce = $model->find()->all(); //equal to SELECT * FROM TABLE hiscores
        return \yii\helpers\Json::encode($responce); 
    }
    public function actionJumper()
    {
        if (Yii::$app->user->isGuest) { //if guest go back to index.php starting page
            return $this->goHome();
        }
        else{
            return $this->render('jumper');
        }
    }
     /*
     * Handless the Ajax-request from jumper.js to save hiscores to database
     */
    public function actionSetscorejumper()
    {
        $model = new Jumperhs();
        //saves different attributes to model
        $model->user_id = Yii::$app->user->identity->user_id;
        $model->username = Yii::$app->user->identity->username;
        $model->score = Yii::$app->request->post("score");
        if($model->validate()){ //validates the model and saves information to database
            $model->save($model->attributes);  
            $success = true;
        }
        else{
            $success = false;
        }
        return $success; // returns true or false depending if succesfull
    }
     /*
     * Handless the Ajax-request from jumper.js to load hiscore table in json
     */
    public function actionGetscorejumper()
    {
        $model = new Jumperhs();
        $responce = $model->find()->all(); //equal to SELECT * FROM TABLE jumperhs
        return \yii\helpers\Json::encode($responce); 
    }

}
