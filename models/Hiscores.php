<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "hiscores".
 *
 * @property integer $hiscore_id
 * @property integer $user_id
 * @property string $username
 * @property integer $score
 */
class Hiscores extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'hiscores';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'username', 'score'], 'required'],
            [['user_id', 'score'], 'integer'],
            [['username'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'hiscore_id' => 'Hiscore ID',
            'user_id' => 'User ID',
            'username' => 'Username',
            'score' => 'Score',
        ];
    }
}
