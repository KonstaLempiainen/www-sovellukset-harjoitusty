<?php

$host = "127.0.0.1";
$port = "11211";

//Code required to make conection to memcached server and check if there is data stored to memcached.
$mem = new Memcache;
$mem->addServer($host, $port) or die("Unable to connect");
$result = $mem->get($_GET["city"] . "weather");


//Incase data is found json is returned back to weather.js
if($result){
    $array = json_decode($result, true);
    array_push($array, array('method' => "Memcached")); //this push is made to make it possible to recognize which method is used
    print_r(json_encode($array));
}
else{//else the data is requested from the openweathermap api and returned back to weather.js
    $json = file_get_contents("http://api.openweathermap.org/data/2.5/weather?q=" . $_GET["city"] . "&APPID=80f3cfcdd0661843c48ca297e25ba2a5");
    $mem->set($_GET["city"] . "weather", $json, null, time() + 60*60); //data is stored to memcached
    $array = json_decode($json, true);
    array_push($array, array('method' => "API"));//this push is made to make it possible to recognize which method is used
    print_r(json_encode($array));
}
