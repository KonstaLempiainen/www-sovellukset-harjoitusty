<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "users".
 *
 * @property integer $user_id
 * @property string $username
 * @property string $password_hash
 * @property string $firstname
 * @property string $lastname
 * @property string $authKey
 */
class Users extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface //ActiveRecord is for working with database and IdentityInterface for user authentication
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'password_hash', 'firstname', 'lastname', 'authKey'], 'required'],
            [['username', 'firstname', 'lastname'], 'string', 'max' => 100],
            [['password_hash'], 'string', 'max' => 255],
            [['authKey'], 'string', 'max' => 60],
            [['username'], 'unique'],
            [['authKey'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'username' => 'Username',
            'password_hash' => 'Password Hash',
            'firstname' => 'Firstname',
            'lastname' => 'Lastname',
        ];
    }

    public function getAuthKey() {//AuthKey could be used for auto-login, but is not utilized so error is thrown incase accidently some1 would try to use it
        throw new \yii\base\NotSupportedException();
    }

    public function getId() {//get user id
        return $this->user_id;
    }

    public function validateAuthKey($authKey) {//AuthKey could be used for auto-login, but is not utilized so error is thrown incase accidently some1 would try to use it
        throw new \yii\base\NotSupportedException();
    }

    public static function findIdentity($id) {//findOne is equal to SELECT FROM TABLE users WHERE ID=$id
        return self::findOne($id);
    }

    public static function findIdentityByAccessToken($token, $type = null) {//AuthKey could be used for auto-login, but is not utilized so error is thrown incase accidently some1 would try to use it
        throw new \yii\base\NotSupportedException();
    }
    public static function findByUsername($username){//findOne is equal to SELECT FROM TABLE users WHERE username=$username
        return self::findOne(['username'=>$username]);
    }
    public function validatePassword($password){//uses password_verify to check if user tried to log in with correct password
        return password_verify($password, $this->password_hash);
    }

}
