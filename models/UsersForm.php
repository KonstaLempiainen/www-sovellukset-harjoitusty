<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "users".
 *
 * @property integer $user_id
 * @property string $username
 * @property string $password_hash
 * @property string $firstname
 * @property string $lastname
 * @property string $authKey
 */
class UsersForm extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     * table users is connected with this model
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * @inheritdoc
     * rules for inputs are created based on table users
     */
    public function rules()
    {
        return [
            [['username', 'password_hash', 'firstname', 'lastname'], 'required'],
            [['username', 'firstname', 'lastname'], 'string', 'max' => 100],
            [['password_hash'], 'string', 'max' => 255],
            [['username'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     * Atribute labels are used in registeration form
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'username' => 'Username',
            'password_hash' => 'Password',
            'firstname' => 'Firstname',
            'lastname' => 'Lastname',
            'authKey' => 'Auth Key',
        ];
    }
}
