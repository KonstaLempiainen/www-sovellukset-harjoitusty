<?php

/* @var $this \yii\web\View */
/* @var $content string */
//in this section the basic layout of the page is created including header, navigation and footer.

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    /*Navigation is mostly modified part of this main.php 
    depending if user is guest or logged in different navigation is shown.*/
    NavBar::begin([
        'brandLabel' => 'Home',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    $menuItems = [
        ['label' => 'Weather', 'url' => ['/site/weather']],
    ];
    if (Yii::$app->user->isGuest) { // if guest show these
        $menuItems[] = ['label' => 'Signup', 'url' => ['/site/signup']];
        $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];
    } else {  //else show all the content
        $menuItems[] = ['label' => 'Games', //the games section is a dropdownlist, that's why multiple different options
                 'items' => [
                 ['label' => 'Singleplayer', 'url' => ['/site/singleplayer']],
                     '<li class="divider"></li>',
                 ['label' => 'Multiplayer', 'url' => ['/site/multiplayer']],
                     '<li class="divider"></li>',
                 ['label' => 'Jumpper', 'url' => ['/site/jumper']],
            ]];
        $menuItems[] = [
            'label' => 'Logout',
            'url' => ['/site/logout'],
            'linkOptions' => ['data-method' => 'post']
        ];
        $menuItems[] = ['label' => '' . Yii::$app->user->identity->username . '', 'url' => ['/site/profile']];
    }
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems,
    ]);
NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer"> <!--Footer is only slightly modified to show my name etc.-->
    <div class="container">
        <p class="pull-left">&copy; Konsta Lempiäinen <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
