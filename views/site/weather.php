<?php

/* @var $this yii\web\View */
/*This viewn contains the weather service. User can download from openwheathermap api or from memcached, the current weather of desired city*/
use yii\helpers\Html;

$this->title = 'Weather';
$this->params['breadcrumbs'][] = $this->title;
$this->registerJsFile('/basic/web/assets/custom/weather.js', ['depends' => [\yii\web\JqueryAsset::className()]]);//weather javascript does the loading of the information using ajax
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        Tässä on openweathermap.com sivustoa hyödyntävä sääpalvelu. Voit hakea haluamasi kaupungin säätiedot:
    </p>
    <input type="text" id="city" placeholder="Kirjoita kaupunki"/>
    <input type="submit" class="btn btn-primary" id="city-submit" value="Lataa" />
    <div id="weather-data"></div>

</div>
