<?php

/* @var $this yii\web\View */
/*This view includes Singleplayer game*/
$this->title = 'Harjoitustyö - Konsta Lempiäinen';
$this->registerJsFile('/basic/web/assets/custom/phaser.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('/basic/web/assets/custom/game2.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
?>
<div class="site-index">

    <div>
        <h1>Singleplayer</h1>

        <p class="lead">Pelaaja liikkuu nuolinäppäimillä. </p>
        

    </div>

    <div class="body-content" id="game">



    </div>
</div>