<?php

/* @var $this yii\web\View */
/*This view includes Multiplayer game. Otherwise page layout is same as Jumper game or Singleplayer game.*/
$this->title = 'Harjoitustyö - Konsta Lempiäinen';
$this->registerJsFile('/basic/web/assets/custom/phaser.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('/basic/web/assets/custom/game.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
?>
<div class="site-index">

    <div>
        <h1>Multiplayer</h1>

        <p class="lead">Pelaaja 1 liikkuu nuolinäppäimillä ja Pelaaja 2 w,a,s,d -näppäimillä. </p>
        

    </div>

    <div class="body-content" id="game">



    </div>
</div>