<?php

/* @var $this yii\web\View */
/*This view includes Jumper game*/
$this->title = 'Harjoitustyö - Konsta Lempiäinen';
$this->registerJsFile('/basic/web/assets/custom/phaser.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //phaser framework javascript is included
$this->registerJsFile('/basic/web/assets/custom/jumper.js', ['depends' => [\yii\web\JqueryAsset::className()]]); //game javascript is included
?>
<div class="site-index">

    <div>
        <h1>Jumper</h1>

        <p class="lead">Pelaaja hyppää hiiren näppäimellä tai kosketusnäyttöä painamalla </p>
        

    </div>

    <div class="body-content" id="game">



    </div>
</div>