<?php

/* @var $this yii\web\View */
/*Index.php is the starting page of my application.*/

$this->title = 'Harjoitustyö - Konsta Lempiäinen';
?>
<div class="site-index">

    <div>
        <h1>Tervetuloa!</h1>

        <p class="lead">Pääset nyt tutustumaan harjoitustyöhöni</p>

    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-6">
                <h2>Sisältö</h2>
                <?php
                if (Yii::$app->user->isGuest) {// here different outputs to the starting page are created depending if use is guest or logged in
                    echo "<p>Sivustolta löytyy muutama aiemmin kurssin aikana tekemäni peli sekä yksi uutuuspeli! Päästäksesi kokeilemaan pelejä, täytyy sinun <a href='index.php?r=site%2Flogin'>kirjautua</a> sisään. Mikäli sinulla ei ole käyttäjätunnusta, voit <a href='index.php?r=site%2Fsignup'>rekisteröityä.</a> Lisäksi tarjolla on yksinkertainen sääpalvelu,
                    josta voit tarkistaa eri paikkakuntien sään. Sääpalvelu ei vaadi rekisteröitymistä.</p>";
                    echo "<p><a class='btn btn-default' href='index.php?r=site%2Fsignup'>Rekisteröidy nyt &rarrhk;</a></p>";
                }
                else{
                    echo "<p>Sivustolta löytyy muutama aiemmin kurssin aikana tekemäni peli sekä yksi uutuuspeli! Lisäksi tarjolla on yksinkertainen sääpalvelu,
                    josta voit tarkistaa eri paikkakuntien sään.</p>";                    
                }
                ?>
            </div>

        </div>

    </div>
</div>
